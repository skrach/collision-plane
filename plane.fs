\ Project Collision Plane
\ Copyright (c) 2013, Michael Tucek <michael@tucek.eu> & Daniel Skrach <daniel@skrach.at>
\ All rights reserved.

require random.fs  \ required for calculating position of asteroids
utime drop seed !  \ seed random number generator with time in microseconds

\ == CONSTANT DEFINITIONS == BEGIN ********************************************
4 constant SHIP_LENGTH            \ x dimension of ship 'sprite' in characters
1 constant BORDER_WIDTH           \ width of asteroid field border in characters
1 constant TITLE_HEIGHT           \ height of title (header) in characters
1 constant FOOTER_HEIGHT          \ height of footer in characters
2 constant STAR_COUNT_DIVISOR     \ asteroid count / STAR_COUNT_DIVISOR stars
10 constant STARHL_POSSIBILITY    \ percent possibility for a star to blink
5 constant STAR_X_MOVE            \ star x move amount to left on level up

\ gameplay dynamics
2 constant ASTEROID_MAX_X_MOVE    \ maximum x-axis movement per step of asteroids in characters
15 constant ASTEROID_INITTURN     \ initial value: every Nth cycle, it's asteroids turn, lower values mean higher initial difficulty
40 constant ASTEROID_WAVE_FACTOR  \ number of simultaneous waves on screen
250 constant LEVEL_THRESHOLD      \ score points needed for next level -- should be asteroidCount related and not a constant to provide similar difficulty on different console dimensions
5 constant VMOVE_MINCYCLES        \ minimum cycles before y move
15 constant VMOVE_MAXCYCLES       \ maximum cycles before y move
15 constant VMOVE_POSSIBILITY     \ percent possibility to have y-axis vector other than 0

\ -1 constant DEBUG               \ debug
0 constant DEBUG                  \ debug
\ == CONSTANT DEFINITIONS == END **********************************************


\ == GLOBAL VARIABLE DEFINITIONS AND INITITALIZATIONS == BEGIN ****************
variable areaWidth             \ screen width
variable areaHeight            \ screen height
variable shipMinX              \ left boundary for ship movement
variable shipMaxX              \ right boundary for ship movement
variable shipMinY              \ top boundary for ship movement
variable shipMaxY              \ bottom boundary for ship movement
variable asteroidStart         \ horizontal origin for asteroids
variable asteroidEnd           \ horizontal sink for asteroids
variable asteroidCount         \ total asteroid count
variable asteroidCountPerWave  \ asteroid count per wave
variable asteroidWaveCycle     \ when to start a new wave
variable asteroidWaveCount     \ number of simultaneous waves on screen
variable engineAlternate       \ used to alternate engine style
variable starCount             \ star count

\ derive content area dimensions from console dimensions
form
areaWidth !
areaHeight !

\ calculate ship movement boundaries
BORDER_WIDTH 1 + shipMinX !                                \ shipMinX = BORDER_WIDTH + 1
areaWidth @ BORDER_WIDTH 1 + - SHIP_LENGTH - shipMaxX !    \ shipMaxX = areaWidth - ( BORDER_WIDTH + 1 ) - SHIP_LENGTH
TITLE_HEIGHT BORDER_WIDTH + shipMinY !                     \ shipMinY = TITLE_HEIGHT + BORDER_WIDTH
areaHeight @ TITLE_HEIGHT BORDER_WIDTH 2 * + - shipMaxY !  \ shipMaxY = areaHeight - ( TITLE_HEIGHT + BORDER_WIDTH * 2 )

\ calculate asteroid origin, sink and total count
areaWidth @ BORDER_WIDTH 1 + - asteroidStart !        \ asteroidStart = areaWidth - ( BORDER_WIDTH + 1 )
BORDER_WIDTH ASTEROID_MAX_X_MOVE 1 - + asteroidEnd !  \ asteroidEnd   = BORDER_WIDTH + ( ASTEROID_MAX_X_MOVE - 1 )

\ asteroidWaveCount = areaWidth / ASTEROID_WAVE_FACTOR
areaWidth @ ASTEROID_WAVE_FACTOR / asteroidWaveCount !
\ asteroidCountPerWave = ( areaHeight - ( BORDER_WIDTH * 2 + TITLE_HEIGHT + FOOTER_HEIGHT + 1 ) )
areaHeight @ BORDER_WIDTH 2 * TITLE_HEIGHT + FOOTER_HEIGHT + 1 + - asteroidCountPerWave !

\ asteroidCount = asteroidCountPerWave * asteroidWaveCount @
asteroidCountPerWave @ asteroidWaveCount @ * asteroidCount !

\ starCount = asteroidCount / STAR_COUNT_DIVISOR
asteroidCount @ STAR_COUNT_DIVISOR / starCount !

\ asteroidWaveCycle = areaWidth / ( asteroidWaveCount @ + 1 )
areaWidth @ asteroidWaveCount @ 1 + / asteroidWaveCycle !

create asteroidsX asteroidCount @ cells allot  \ positions of asteroids on x-axis
create asteroidsY asteroidCount @ cells allot  \ positions of asteroids on y-axis
create asteroidsV asteroidCount @ cells allot  \ vector of asteroids on y-axis
create asteroidsM asteroidCount @ cells allot  \ cycles since last move on y-axis
create starsX starCount @ cells allot          \ positions of stars on x-axis
create starsY starCount @ cells allot          \ positions of stars on y-axis

\ game specific variables, which have to be reset for a new game
variable running                   \ whether the game is running
variable paused                    \ whether the game is paused or not
variable score                     \ current game's score
variable shipX                     \ ship position on x-axis
variable shipY                     \ position of ship on y-axis
variable asteroidCycleCounter      \ cycle counter to determin asteroid turn
variable asteroidTurn              \ every Nth cycle, it's asteroids turn, lower values means higher initial difficulty
variable asteroidWaveCycleCounter  \ counts cycles to know when to start a new wave
variable currentWave               \ counts up until all wave have been started
variable currentLevel              \ current level
variable levelScore                \ current level score

\ == GLOBAL VARIABLE DEFINITIONS AND INITITALIZATIONS == END ******************


\ == COLOR DEFINITIONS == BEGIN ***********************************************
: console-reset ( -- )
    .\" \033[0m"  \ reset console style
;

: console-bgfield ( -- )
    .\" \033[40m"  \ black bg
;

: console-fgasteroid ( -- )
    .\" \033[33m"  \ brown fg
;

: console-fgstar ( -- )
    .\" \033[37m"  \ gray fg
;

: console-fgstarhl ( -- )
    .\" \033[38m"  \ white fg
;

: console-fgexplosion ( -- )
    .\" \033[31m"  \ red fg
;

: console-bggameover ( -- )
    .\" \033[44m"  \ blue bg
;

: console-fggameover ( -- )
    .\" \033[31;1m"  \ red fg
;

: console-fgship ( -- )
    .\" \033[36;1m"  \ light blue fg
;

: console-fgshipnose ( -- )
    .\" \033[36;1m"  \ light blue fg
;

: console-fgshipengine1 ( -- )
    .\" \033[34;1m"  \ blue fg
;

: console-fgshipengine2 ( -- )
    .\" \033[31;1m"  \ red fg
;

: console-bgborder ( -- )
    console-bgfield
;

: console-fgborder-h ( -- )
    .\" \033[38m"  \ white fg
;

: console-fgborder-v ( -- )
    .\" \033[37m"  \ gray fg
;

: console-bgtitle ( -- )
    .\" \033[44m"  \ blue bg
;

: console-fgtitle ( -- )
    .\" \033[38;1m"  \ white and bold fg
;

: console-bgfooter ( -- )
    .\" \033[44m"  \ blue bg
;

: console-fgfooter ( -- )
    .\" \033[38;1m"  \ white and bold fg
;

: console-bgscore ( -- )
    .\" \033[46m"  \ light blue bg
;

: console-fgscore ( -- )
    .\" \033[38;1m"  \ white and bold fg
;

: console-bglevel ( -- )
    .\" \033[42m"  \ green bg
;

: console-fglevel ( -- )
    .\" \033[38;1m"  \ white and bold fg
;
\ == COLOR DEFINITIONS == END *************************************************


: console-clear ( -- )
    console-bgfield  \ set bg color
    page             \ clear console
    console-reset    \ reset console style
;

: cursor-idle ( -- )
    areaWidth @ 1 - areaHeight @ 1 - at-xy
;

: draw-score ( -- )
    console-bgfield  \ set bg color

    areaWidth @ 15 - 1 at-xy
    space
    areaWidth @ 15 - 0 at-xy
    space

    console-bgscore  \ set bg color
    console-fgscore  \ set fg color

    ."  Score:       "
    areaWidth @ 6 - 0 at-xy
    score @ .

    console-reset  \ reset console style
    cursor-idle    \ set cursor to idle position
;

: draw-level ( -- )
    console-bgfield  \ set bg color

    areaWidth @ 27 - 1 at-xy
    space
    areaWidth @ 27 - 0 at-xy
    space

    console-bglevel  \ set bg color
    console-fglevel  \ set fg color

    ."  Level:    "
    areaWidth @ 18 - 0 at-xy
    currentLevel @ .

    console-reset  \ reset console style
    cursor-idle    \ set cursor to idle position
;

: output-line { addr l y -- } ( addr l y -- )
    addr l       \ repush string
    0 y at-xy    \ set cursor to left position
    type         \ output title text
    areaWidth @ l
    2dup > if
        do i y at-xy space loop  \ draw rest of line blank
    else
        2drop
    then
;

: title ( -- )
    console-bgtitle  \ set bg color
    console-fgtitle  \ set fg color

    \ push great game title
    s"  Asteroid Field Of DOOOOM!!!"

    0 output-line  \ output line padded with spaces
    console-reset  \ reset console style
    cursor-idle    \ set cursor to idle position
;

: footer ( -- )
    console-bgfooter  \ set bg color
    console-fgfooter  \ set fg color

    \ push controls help
    s"  W = UP | S = DOWN | A = LEFT | D = RIGHT | X = SUICIDE | P = PAUSE | Q = QUIT"

    areaHeight @ 1 - output-line  \ output line padded with spaces
    console-reset                 \ reset console style
    cursor-idle                   \ set cursor to idle position
;

: footer-pause ( -- )
    console-bgfooter  \ set bg color
    console-fgfooter  \ set fg color

    \ push controls help
    s"  ANY KEY = UNPAUSE"

    areaHeight @ 1 - output-line  \ output line padded with spaces
    console-reset                 \ reset console style
    cursor-idle                   \ set cursor to idle position
;

: footer-gameover ( -- )
    console-bgfooter  \ set bg color
    console-fgfooter  \ set fg color

    \ push controls help
    s"  Q = QUIT | R = RESTART"

    areaHeight @ 1 - output-line  \ output line padded with spaces
    console-reset                 \ reset console style
    cursor-idle                   \ set cursor to idle position
;

: draw-border ( -- )
    console-bgborder
    console-fgborder-h

    areaWidth @ 0 do i 1                at-xy 175 xemit loop  \ top border
    areaWidth @ 0 do i areaHeight @ 2 - at-xy  95 xemit loop  \ bottom border

    console-reset
    console-bgborder
    console-fgborder-v

    areaHeight @ 2 - 2 do 0 i               at-xy 171 xemit loop  \ left border
    areaHeight @ 2 - 2 do areaWidth @ 1 - i at-xy 171 xemit loop  \ right border
;

: draw-engine ( -- )
    shipX @ shipY @ at-xy       \ set cursor position
    console-bgfield             \ set asteroid field bg
    engineAlternate @ if
        console-fgshipengine1   \ set ship engine fg
        187 xemit               \ draw ship engine
        0 engineAlternate !
    else
        console-fgshipengine2   \ set ship engine fg
        187 xemit               \ draw ship engine
        -1 engineAlternate !
    then

    console-reset               \ reset console style
;

: draw-ship ( -- )
    draw-engine

    console-bgfield     \ set asteroid field bg
    console-fgship      \ set ship fg
    926 xemit 41 xemit  \ draw ship
    console-fgshipnose
    62 xemit            \ draw ship nose
    console-reset       \ reset console style

    cursor-idle         \ set cursor to idle position
;

: erase-ship ( -- )
    shipX @ shipY @ at-xy SHIP_LENGTH 0 do space loop  \ erase current location
;

: move-ship-v ( n -- )
    console-bgfield  \ set asteroid field bg

    erase-ship

    \ add top stack element to shipY
    shipY @ + shipY !
    draw-ship
;

: move-ship-h ( n -- )
    console-bgfield  \ set asteroid field bg

    erase-ship

    \ add top stack element to shipX
    shipX @ + shipX !
    draw-ship
;

: game-over ( -- )
    0 running !  \ game over

    \ explosion
    console-bgfield
    console-fgexplosion

    shipX @ 1 - shipY @     at-xy 164 xemit

    shipX @     shipY @ 2 - at-xy 164 xemit
    shipX @ 1 + shipY @ 1 - at-xy 164 xemit

    shipX @ 2 + shipY @ 2 - at-xy 164 xemit
    shipX @ 2 + shipY @ 3 - at-xy 164 xemit

    shipX @ 3 + shipY @ 1 - at-xy 164 xemit
    shipX @ 4 + shipY @ 2 - at-xy 164 xemit

    shipX @ 4 + shipY @ 1 - at-xy 164 xemit
    shipX @ 6 + shipY @ 2 - at-xy 164 xemit

    shipX @ 5 + shipY @     at-xy 164 xemit
    shipX @ 7 + shipY @     at-xy 164 xemit

    shipX @ 1 - shipY @ 3 + at-xy 164 xemit
    shipX @ 1 + shipY @ 1 + at-xy 164 xemit

    shipX @ 2 + shipY @ 1 + at-xy 164 xemit
    shipX @ 2 + shipY @ 2 + at-xy 164 xemit
    shipX @ 2 + shipY @ 3 + at-xy 164 xemit

    shipX @ 3 + shipY @ 1 + at-xy 164 xemit
    shipX @ 4 + shipY @ 2 + at-xy 164 xemit

    shipX @ 4 + shipY @ 1 + at-xy 164 xemit
    shipX @ 6 + shipY @ 2 + at-xy 164 xemit

    console-fgship

    shipX @ 1 + shipY @     at-xy 47 xemit
    shipX @ 2 + shipY @ 1 - at-xy 126 xemit
    shipX @ 2 + shipY @     at-xy 709 xemit
    shipX @     shipY @ 2 + at-xy 96 xemit


    \ game-over text
    console-bggameover
    console-fggameover

    s"                               "
    dup
    areaWidth @ 2 / swap 2 / - areaHeight @ 2 / 2 - at-xy
    type

    s"   BOOOOOOOOOOOOM !!!!!!!!!!!  "
    dup
    areaWidth @ 2 / swap 2 / - areaHeight @ 2 / 1 - at-xy
    type

    s"           GAME OVER.          "
    dup
    areaWidth @ 2 / swap 2 / - areaHeight @ 2 / at-xy
    type

    s"                               "
    dup
    areaWidth @ 2 / swap 2 / - areaHeight @ 2 / 1 + at-xy
    type

    console-reset

    footer-gameover  \ draw control help
;

\ callmap address helpers
: cell-helper
    swap cells +
;

: asteroid-x ( n -- addr )
    asteroidsX cell-helper
;

: asteroid-y ( n -- addr )
    asteroidsY cell-helper
;

: asteroid-v ( n -- addr )
    asteroidsV cell-helper
;

: asteroid-m ( n -- addr )
    asteroidsM cell-helper
;

: star-x ( n -- addr )
    starsX cell-helper
;

: star-y ( n -- addr )
    starsY cell-helper
;

\ asteroid positioning
: position-asteroid { a -- }
    a asteroid-x @
    a asteroid-y @
    at-xy
;

: draw-asteroid { a -- }
    a position-asteroid
    35 xemit
\    164 xemit
;

: erase-asteroid { a -- }
    a position-asteroid
    space
;

: draw-asteroids { a -- }
    console-bgfield
    console-fgasteroid

    asteroidCountPerWave @ a 1 + * asteroidCountPerWave @ a * do
        i draw-asteroid
    loop

    cursor-idle  \ set cursor to idle position
;

\ star positioning
: position-star { a -- }
    a star-x @
    a star-y @
    at-xy
;

\ hl reset flag
variable starhlreset
0 starhlreset !

: draw-star { a -- }
    a position-star

    100 random STARHL_POSSIBILITY <= if
        console-fgstarhl
        -1 starhlreset !
        43 xemit
    else
        183 xemit
    then

    starhlreset @ if
        console-fgstar
        0 starhlreset !
    then
;

: erase-star { a -- }
    a position-star
    space
;

: move-stars ( -- )
    console-bgfield
    console-fgstar

    starCount @ 0 do
        i erase-star

        \ calculate x-movement
        i star-x @ STAR_X_MOVE - i star-x !  \ move asteroid between 1 and 2 characters to the left

        \ reset star if it is moving out of level
        i star-x @ shipMinX @ < if
            shipMaxX @ i star-x !
        then
    loop

    cursor-idle  \ set cursor to idle position
;

: draw-stars ( -- )
    console-bgfield
    console-fgstar

    starCount @ 0 do
        i star-y @ shipY @ <> if
            i draw-star
        else
            shipX @ SHIP_LENGTH + 1 - i star-x @ < if
                i draw-star
            else
                shipX @ i star-x @ > if
                    i draw-star
                then
            then
        then
    loop

    cursor-idle  \ set cursor to idle position
;

: random-x ( -- n )
    shipMaxX @ shipMinX @ - 1 + random shipMinX @ +
;

: random-y ( -- n )
    shipMaxY @ shipMinY @ - 1 + random shipMinY @ +
;

: random-asteroid-v ( -- n )
    100 random VMOVE_POSSIBILITY <= if
        VMOVE_MAXCYCLES VMOVE_MINCYCLES - 1 + random VMOVE_MINCYCLES +
        100 random 50 <= if
            negate
        then
    else
        0
    then
;

: draw-scene ( -- )
    console-clear  \ clear screen with correct bg color
    title          \ output title
    footer         \ output footer
    draw-border    \ output asteroid field border
    draw-score     \ draw score
    draw-level     \ draw level
    draw-stars     \ draw stars initially
    draw-ship      \ draw ship initially
;

\ score routine
: asteroid-avoided { i -- }
    1 score @ + score !                \ increment score, because asteroid has been avoided
    1 levelScore @ + dup levelScore !  \ increment level score
    LEVEL_THRESHOLD = if
        0 levelScore !                         \ reset level score
        currentLevel @ 1 + currentLevel !      \ increment level
        asteroidTurn @ 1 > if
            asteroidTurn @ 1 - asteroidTurn !  \ increase difficulty by 1
        then

        STAR_X_MOVE 0 do
            move-stars
            draw-stars
            40 ms
        loop        

        draw-level
        console-bgfield
        console-fgasteroid
    then

    asteroidStart @ i asteroid-x !    \ reset asteroid on x-axis
    random-y i asteroid-y !  \ randomize asteroid on y-axis
    random-asteroid-v i asteroid-v !  \ randomize asteroid y-axis vector
    0 i asteroid-m !                  \ reset y-axis cycle movement counter
;

\ asteroid movement
: move-asteroid { i -- }
    i erase-asteroid
    i asteroid-x @ dup asteroidEnd @ > if
        \ calculate x-movement
        ASTEROID_MAX_X_MOVE random 1 + - i asteroid-x !  \ move asteroid between 1 and 2 characters to the left

        \ calculate y-movement
        i asteroid-v @ 0 <> if
            i asteroid-m @ i asteroid-v @ abs <= if
                \ i asteroid-x @ i asteroid-y @ at-xy ." **"  \ debug trail
                i asteroid-m @ 1 + i asteroid-m !  \ increment y-axis cycle counter
            else
                0 i asteroid-m !
                i asteroid-v @ 0 <= if
                    i asteroid-y @ 1 - i asteroid-y !
                else
                    i asteroid-y @ 1 + i asteroid-y !
                then
            then
        then

        i asteroid-y @ shipMinY @ < if
            i asteroid-avoided
        else
            i asteroid-y @ shipMaxY @ > if
                i asteroid-avoided
            else
                i draw-asteroid

                \ check collission
                shipY @ i asteroid-y @ = if
                    shipX @ SHIP_LENGTH + 1 - i asteroid-x @ >= if
                        shipX @ i asteroid-x @ <= if
                            game-over
                        then
                    then
                then
            then
        then
    else
        drop
        i asteroid-avoided
    then
;

: move-asteroids ( -- )
    console-bgfield
    console-fgasteroid

    currentWave @ 1 + 0 do
        asteroidCountPerWave @ 0 do
            asteroidCountPerWave @ j * i + move-asteroid
            running @ 0 = if
                leave  \ leave on collision detected
            then
        loop
        running @ 0 = if
            leave  \ leave on collision detected
        then
    loop

    \ next wave if not all are launched yet
    currentWave @ asteroidWaveCount @ 1 - <> if
        asteroidWaveCycleCounter @ asteroidWaveCycle @ = if
            currentWave @ 1 + currentWave !  \ launch wave in next turn
            0 asteroidWaveCycleCounter !     \ reset wave cycle counter
            currentWave @ draw-asteroids     \ draw initial asteroids for new wave
        else
            asteroidWaveCycleCounter @ 1 + asteroidWaveCycleCounter !  \ increase wave cycle counter
        then
    then

    console-reset  \ reset console style
    draw-score     \ draw new score
    cursor-idle    \ set cursor to idle position
;

\ game variable initialization
: reset-game ( -- )
    shipMinX @ shipX !                \ move ship to left border
    areaHeight @ 2 / shipY !          \ center ship on y-axis
    1 negate running !                \ set game is running
    0 paused !                        \ set game is not paused
    0 score !                         \ set score to zero
    0 asteroidCycleCounter !          \ reset asteroid cycle counter
    ASTEROID_INITTURN asteroidTurn !  \ reset difficulty
    0 asteroidWaveCycleCounter !      \ reset asteroid wave cycle counter
    0 currentWave !                   \ reset wave counter
    0 currentLevel !                  \ reset level counter
    0 levelScore !                    \ reset level score counter

    \ generate random y positions for asteroids
    asteroidCount @ 0 do
        random-y  \ generate a value between shipMinY and shipMaxY
        i asteroid-y !
    loop

    \ set all asteroids to start position
    asteroidCount @ 0 do
        asteroidStart @
        i asteroid-x !
    loop

    \ set asteroid y-axis vectors
    asteroidCount @ 0 do
        random-asteroid-v  \ generate vector value
        i asteroid-v !
    loop

    \ reset asteroid y-axis movement cycle counter
    asteroidCount @ 0 do
        0
        i asteroid-m !
    loop

    \ generate random x positions for stars
    starCount @ 0 do
        random-x  \ generate a value between shipMinX and shipMaxX
        i star-x !
    loop

    \ generate random y positions for stars
    starCount @ 0 do
        random-y  \ generate a value between shipMinY and shipMaxY
        i star-y !
    loop

	\ game setup debug
    DEBUG if
        ." SHIP_LENGTH: " SHIP_LENGTH . cr
        ." BORDER_WIDTH: " BORDER_WIDTH . cr
        ." TITLE_HEIGHT: " TITLE_HEIGHT . cr
        ." FOOTER_HEIGHT: " FOOTER_HEIGHT . cr
        ." STAR_COUNT_DIVISOR: " STAR_COUNT_DIVISOR . cr
        ." STARHL_POSSIBILITY: " STARHL_POSSIBILITY . cr
        ." STAR_X_MOVE: " STAR_X_MOVE . cr
        ." ASTEROID_MAX_X_MOVE: " ASTEROID_MAX_X_MOVE . cr
        ." ASTEROID_INITTURN: " ASTEROID_INITTURN . cr
        ." ASTEROID_WAVE_FACTOR: " ASTEROID_WAVE_FACTOR . cr
        ." LEVEL_THRESHOLD: " LEVEL_THRESHOLD . cr
        ." VMOVE_MINCYCLES: " VMOVE_MINCYCLES . cr
        ." VMOVE_MAXCYCLES: " VMOVE_MAXCYCLES . cr
        ." VMOVE_POSSIBILITY: " VMOVE_POSSIBILITY . cr

        ." areaWidth: " areaWidth @ . cr
        ." areaHeight: " areaHeight @ . cr
        ." shipMinX: " shipMinX @ . cr
        ." shipMaxX: " shipMaxX @ . cr
        ." shipMinY: " shipMinY @ . cr
        ." shipMaxY: " shipMaxY @ . cr
        ." asteroidStart: " asteroidStart @ . cr
        ." asteroidEnd: " asteroidEnd @ . cr
        ." asteroidWaveCount: " asteroidWaveCount @ . cr
        ." asteroidCount: " asteroidCount @ . cr
        ." asteroidCountPerWave: " asteroidCountPerWave @ . cr
        ." asteroidWaveCycle: " asteroidWaveCycle @ . cr
        ." engineAlternate: " engineAlternate @ . cr
        ." starCount: " starCount @ . cr
        ." running: " running @ . cr
        ." paused: " paused @ . cr
        ." score: " score @ . cr
        ." shipX: " shipX @ . cr
        ." shipY: " shipY @ . cr
        ." asteroidCycleCounter: " asteroidCycleCounter @ . cr
        ." asteroidTurn: " asteroidTurn @ . cr
        ." asteroidWaveCycleCounter: " asteroidWaveCycleCounter @ . cr
        ." currentWave: " currentWave @ . cr
        ." currentLevel: " currentLevel @ . cr
        ." levelScore: " levelScore @ . cr
        .s
        key drop
        ." asteroidsX:"
        asteroidsX asteroidCount @ cells dump
        key drop
        ." asteroidsY:"
        asteroidsY asteroidCount @ cells dump
        key drop
        ." asteroidsV:"
        asteroidsV asteroidCount @ cells dump
        key drop
        ." asteroidsM:"
        asteroidsM asteroidCount @ cells dump
        key drop
        ." starsX:"
        starsX starCount @ cells dump
        key drop
        ." starsY:"
        starsY starCount @ cells dump
        key drop
    then

    draw-scene                    \ (re-)draw interface
    currentWave @ draw-asteroids  \ inital asteroids
;

\ user interaction
: end ( -- )
    console-reset
    page
    bye
;

: plane-up ( -- )
    shipY @ shipMinY @ > if
        -1 move-ship-v
    then
;

: plane-down
    shipY @ shipMaxY @ < if
        1 move-ship-v
    then
;

: plane-left ( -- )
    shipX @ shipMinX @ > if
        -1 move-ship-h
    then
;

: plane-right
    shipX @ shipMaxX @ < if
        1 move-ship-h
    then
;

: process-game-key
    case
        105 ( i ) of  \ debug star animation
            STAR_X_MOVE 0 do
                move-stars
                draw-stars
                30 ms
            loop 
            endof
        113 ( q ) of
            end
            endof
        119 ( w ) of
            plane-up
            endof
        115 ( s ) of
            plane-down
            endof
        97 ( a ) of
            plane-left
            endof
        100 ( d ) of
            plane-right
            endof
        120 ( x ) of
    	    shipX @ SHIP_LENGTH + shipY @ at-xy  \ position cursor to ship nose
            game-over
            endof
        112 ( p ) of
            1 negate paused !  \ pause game
            footer-pause       \ draw pause footer
            endof
    endcase
;

: process-gameover-key
    case
        113 ( q ) of
            end
            endof
        114 ( r ) of
            reset-game
            endof
    endcase
;

: process-unpause-key
    0 paused !  \ unpause game
    footer      \ redraw game footer
    drop        \ drop key stroke
;

\ the loop
: start
    reset-game

    begin
        paused @ if
            key process-unpause-key
        else
            running @ if
                key? if
                    key process-game-key
                then
                draw-engine
                cursor-idle
                asteroidCycleCounter @ 1 + dup asteroidCycleCounter !  \ increment cycle counter and leave dup for comparison
                asteroidTurn @ = if
                    draw-stars
                    move-asteroids            \ asteroid's turn
                    0 asteroidCycleCounter !  \ reset cylce counter
                then
            else
                begin
                    key process-gameover-key
                    running @
                until
            then
        then
        10 ms
    again
;

start

\ ENDPRÄSENTATION Debug herzeigen (die Daten hinter einer Spielrunde)
\  - Wichtig für die endpräsentation: antwort auf die frage welche sprachspezifischen features man verwendet hat und besonders interessante stellen des sourcecodes herzeigen