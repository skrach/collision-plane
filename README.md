# Collision Plane
Also known as *Asteroid Field of DOOOOM!!!* is an addictive space plane game written in [gforth(1)](http://www.complang.tuwien.ac.at/forth/gforth/).

![Collision plane screenshot](https://bitbucket.org/skrach/collision-plane/raw/334c5c4bea0140ed5b20cb6b752e4bf14e2f31f2/inline-image.png)

## Gameplay
The game is easily explained: You fly a plane through space and try to avoid crashing with asteroids flying upon you. The plane is controlled using the keyboard (keys *a,w,d,s*). Avoiding asteroids keeps you alive and lets you earn points for your highscore! But be aware! The better your score, the harder it gets as you reach the next level. Keep cool and be warned that Collision Plane is highly addictive!

## Features
Buckle up and start reading!

* colors!
* asteroids are random
* score and level display
* increasing difficulty depending upon score
* pause mode
* UTF-8 support
* engine exhaust animation
* horizontal and vertical asteroid movement
* adapts to terminal size
* extremely addictive

## Installation
1. Get [gforth(1)](http://www.complang.tuwien.ac.at/forth/gforth/) for your system
2. Set your terminal to UTF-8 and ensure that ANSI colors are enabled and correctly displayed
3. Start the game:

        $ gforth plane.fs
        
## Contribution
You have an idea to make this game even better? Great! We are glad you want to help!

It's easy:

1. Fork the project
2. Make your changes
3. Send us a pull request
        
## License
It is open source and covered by a standard BSD license. That means you have to mention the original authors of this code. For details see the `LICENSE` file.

The [logo](http://pixabay.com/en/black-model-outline-silhouette-26556/) is licensed under public domain. 
